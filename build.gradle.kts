import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    kotlin("jvm") version "1.3.61"
    application
}

val mainClass = "at.race.prediction.AppKt"

repositories {
    mavenCentral()
    maven(url = "https://dl.bintray.com/arrow-kt/arrow-kt/")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("io.javalin:javalin:3.6.0")
    implementation("com.google.code.gson:gson:2.8.5")
    implementation("com.natpryce:krouton:2.3.1.3")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.1")
    implementation("org.slf4j:slf4j-simple:1.7.28")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:2.13.0")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.10.1")
    implementation("com.google.code.gson:gson:2.8.6")
}

application {
    mainClassName = mainClass
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    test {
        useJUnitPlatform()

        testLogging {
            exceptionFormat = TestExceptionFormat.FULL
        }
    }

    jar {
        manifest {
            attributes["Main-Class"] = mainClass
            attributes["Implementation-Title"] = "Race Prediction"
        }

        from(configurations.runtimeClasspath.map { elements ->
            elements.files.map {
                if (it.isDirectory) it
                else zipTree(it)
            }
        })

        from(file("src/main/resources/log4j.xml"))
    }
}

