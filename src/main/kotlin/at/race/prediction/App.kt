package at.race.prediction

import io.javalin.Javalin
import io.javalin.http.Context
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.time.ExperimentalTime


class App {}

val log: Logger = LoggerFactory.getLogger(App::class.java)

@ExperimentalTime
fun main () {
    Javalin.create()
            .addGSON()
            .get("/") { context ->
                context.racePrediction().apply {
                    context.json(prediction(race, previousResults))
                }
            }
            .exception(Exception::class.java) { e: Exception, ctx: Context ->
                log.error("Failure", e)
                ctx.result("Internal Failure")
            }
            .start(7000)
}
















