package at.race.prediction

import java.math.BigDecimal
import java.math.RoundingMode

fun percentage(baseValue: Long, overallPace: Long) : Double =
    (100.0 - ((baseValue.toDouble() / overallPace.toDouble()) * 100)) * -1

fun Double.round(decimals: Int): Double = BigDecimal(this).setScale(decimals, RoundingMode.HALF_EVEN).toDouble()

fun Long.leadingZeros(zeros: Int = 2) = this.toString().padStart(zeros, '0')