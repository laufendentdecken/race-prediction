package at.race.prediction.model

data class RacePrediction (
        val race: Race,
        val previousResults: List<PreviousResult>
)