package at.race.prediction.model

import java.time.LocalTime

data class PreviousResult (
    val time: LocalTime,
    val runTimes: List<RunTime>
)

typealias RunTime = LocalTime


data class Race(
    val distance : Double,
    val preferredTime: LocalTime,
    val aidStations: List<AidStation>
) {

    fun overallPace() = pace(distance, preferredTime)
}

data class AidStation (
    val number: Number,
    val name: String,
    val distance: Double
)
