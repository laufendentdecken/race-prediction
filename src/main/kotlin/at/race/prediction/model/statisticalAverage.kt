package at.race.prediction.model

import at.race.prediction.percentage
import at.race.prediction.round
import java.time.LocalTime

data class StatisticalAverage (
    val averagePerSegment: List<Percentage>
)

typealias Percentage = Double

fun statisticalAverage(
    segments: List<AidStation>,
    results: List<PreviousResult>,
    distance: Double
) = StatisticalAverage(
    averagePerSegment = toAveragePacePerSegmentList(
        segments = segments,
        results = results,
        distance = distance,
        segment = 0
    )
)

private fun toAveragePacePerSegmentList(
    segments: List<AidStation>,
    results: List<PreviousResult>,
    distance: Double,
    segment: Int
) : List<Percentage> =
    when {
        results[0].runTimes.size > segment -> {
            mutableListOf(
                results.map {
                    percentage(
                        baseValue = pace(
                            segments[segment].distance,
                            it.runTimes[segment]
                        ).toSeconds(),

                        overallPace = pace(
                            distance = distance,
                            time = it.time
                        ).toSeconds()
                    )
                }

                .average()
                .round(2)
            ).also {
                it.addAll(toAveragePacePerSegmentList(
                    segments = segments,
                    results = results,
                    distance = distance,
                    segment = segment + 1
                ))
            }
        }

        else -> mutableListOf()
    }



fun pace(distance: Double, time: LocalTime): Pace = Pace (
    minutes = time.toMinutes(),
    distance = distance
)
