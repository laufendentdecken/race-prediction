package at.race.prediction.model

import at.race.prediction.round
import java.time.LocalTime
import kotlin.time.ExperimentalTime

@ExperimentalTime
data class Result (
    val aidStation: String,
    val distance: Double,
    val time: LocalTime,
    val pace: Pace
) {
    constructor(aidStation: String, distance: Double, pace: Pace) : this (
        aidStation = aidStation,
        distance = distance,
        time = LocalTime.ofSecondOfDay((distance * pace.toSeconds()).round(0).toLong()),
        pace = pace
    )
}
