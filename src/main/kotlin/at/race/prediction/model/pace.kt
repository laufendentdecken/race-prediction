package at.race.prediction.model

import at.race.prediction.leadingZeros
import at.race.prediction.round
import java.time.Duration
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.minutes
import kotlin.time.seconds

data class Pace (
    val minutes: Long,
    val seconds: Long,
    val unit: PaceUnit = PaceUnit.MIN_KM
) {

    constructor(minutes: Double, distance: Double) : this(
        minutes = (minutes / distance).toLong(),
        seconds = (60 * ((minutes / distance) % (minutes / distance).toLong())).round(0).toLong()
    )

    @ExperimentalTime
    constructor(seconds: Long) : this(
        minutes = (seconds.seconds.toLong(DurationUnit.MINUTES)),
        seconds = (seconds - (seconds.seconds.toLong(DurationUnit.MINUTES) * 60))
    )


    enum class PaceUnit(val description: String ) {
        MIN_KM("min/km"), MILES_PER_HOUR("mph")
    }

    fun toSeconds() = (minutes * 60) + seconds

    override fun toString() = "$minutes:${seconds.leadingZeros()} ${unit.description}"
}

fun LocalTime.toMinutes() : Double = (hour * 60) + minute + (second.toDouble() / 60)
fun String.asTime() : LocalTime = LocalTime.parse(this, DateTimeFormatter.ofPattern("HH:mm:ss"))
