package at.race.prediction

import at.race.prediction.model.*
import kotlin.time.ExperimentalTime

@ExperimentalTime
fun prediction(race: Race, results: List<PreviousResult>) =
        statisticalAverage(race.aidStations, results, race.distance).use {
            race.aidStations.mapIndexed { index, aidStation ->
                Result(
                        aidStation = race.aidStationDescription(index),
                        distance = aidStation.distance,
                        pace = Pace(
                                seconds = (race.overallPace().toSeconds() * deviation(this.averagePerSegment[index]))
                                        .round(0)
                                        .toLong()
                        )
                )
            }
        }

private fun Race.aidStationDescription(index: Int): String = when (index) {
    0 -> "Start to ${this.aidStations[index].name}"
    else -> "${this.aidStations[index-1].name} to ${this.aidStations[index].name}"
}

@ExperimentalTime
private fun StatisticalAverage.use(block: StatisticalAverage.() -> List<Result>) = block()

private fun deviation(percentage: Double) =
        (percentage / 100) + 1.0