package at.race.prediction

import at.race.prediction.model.Pace
import at.race.prediction.model.RacePrediction
import com.google.gson.*
import io.javalin.Javalin
import io.javalin.http.Context
import io.javalin.plugin.json.FromJsonMapper
import io.javalin.plugin.json.JavalinJson
import io.javalin.plugin.json.ToJsonMapper
import java.lang.reflect.Type
import java.time.LocalTime
import java.time.format.DateTimeFormatter

fun Context.racePrediction() =
        JavalinJson.fromJson(this.body(), RacePrediction::class.java)

fun Javalin.addGSON(): Javalin {
    GsonBuilder()
            .registerTypeAdapter(LocalTime::class.java, JsonDeserializer<LocalTime> { json: JsonElement, _, _ ->
                LocalTime.parse(json.asJsonPrimitive.asString, DateTimeFormatter.ISO_LOCAL_TIME)
            })
            .registerTypeAdapter(Pace::class.java, JsonSerializer<Pace> { pace: Pace, _, _ ->
                JsonPrimitive("${pace.minutes.leadingZeros()}:${pace.seconds.leadingZeros()} ${pace.unit.description}")
            })
            .registerTypeAdapter(LocalTime::class.java, JsonSerializer<LocalTime> { localTime: LocalTime, _, _ ->
                JsonPrimitive(localTime.format(DateTimeFormatter.ISO_LOCAL_TIME))
            })
            .create()
            .also {
                JavalinJson.fromJsonMapper = object : FromJsonMapper {
                    override fun <T> map(json: String, targetClass: Class<T>) = it.fromJson(json, targetClass)
                }

                JavalinJson.toJsonMapper = object : ToJsonMapper {
                    override fun map(obj: Any): String = it.toJson(obj)
                }
            }
    return this
}

